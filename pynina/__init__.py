__version__ = "0.3.4"
__author__ = "DeerMaximum"

from .baseApi import ApiError
from .nina import Nina
from .warning import Warning
